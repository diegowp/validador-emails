<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 29/06/2017
 * Time: 21:34
 */

namespace Dwp\Email;


class Email
{
    public function getConfig( $section )
    {
        $config = parse_ini_file( "config.ini", true );
        return $config[ $section ];
    }

    public function stringErrors( $email )
    {
        $commomErrors = array();

        foreach ( $this->getConfig("Emails") as $k => $v ){
            $commomErrors[$k] = $v;
        }

        $email = $this->emailCleaner( $email );

        $getDomain = explode( "@", $email );

        if( !empty( $getDomain ) && count( $getDomain ) == 2 ){

            if( array_key_exists( $getDomain[1], $commomErrors ) ){
                $newEmail = $getDomain[ 0 ]."@".$commomErrors[ $getDomain[1] ];
            }else{
                $newEmail = $getDomain[0]."@".$getDomain[1];
            }

        }else{
            $newEmail = "invalido@invalido.dwp";
        }

        return $newEmail;

    }

    public function checkDns( $email )
    {
        $getDomain = explode( "@", $email );
        if( !empty( $getDomain ) ){

            if( checkdnsrr( $getDomain[1], 'ANY') ){
                return "Válido";
            }else{
                return "Inválido";
            }

        }else{
            return "Inválido";
        }
    }

    public function getFiles()
    {
        $config = $this->getConfig("DefaultDir");
        $files = scandir( $config['files'] );
        $result = array();

        foreach ( $files as $v ){
            $infos = pathinfo( $v );
            if( $infos['extension'] == "csv" ){
                array_push( $result, $v );
            }
        }

        return $result;
    }

    public function emailCleaner( $email )
    {
        $email = trim( $email );
        $email = str_replace( " ", "", $email );
        $email = str_replace( ",", ".", $email );
        $email = stripcslashes( $email );
        $email = str_replace( "-", "_", $email );
        $email = strtolower( $email );

        return $email;
    }

    public function openFile( $file_name, $colEmails )
    {
        $config = $this->getConfig("DefaultDir");
        $file       = $config['files'] . $file_name;
        $open_file  = file( $file );
        $saveNewList = array();

        // Remove ;
        $removeSemicolon = str_replace( ";", ",", $open_file[0] );
        $splitHeaders = explode( ",", $removeSemicolon );

        // Remove column "Email" from Headers
        unset( $splitHeaders[$colEmails] );

        // Mount new columns
        $getHeaders = implode( ",", $splitHeaders ) . ",Email,É válido?";

        // Remove first line with headers
        unset( $open_file[0] );

        // Save new columns in new file
        array_push( $saveNewList, utf8_decode( $getHeaders ) );

        foreach ( $open_file as $value ){
            // Remove ;
            $removeSemicolon = str_replace( ";", ",", $value );
            $list = explode( ",", str_replace( ";", ",", $removeSemicolon ) );

            if( !empty( $list[ $colEmails ] ) ){
                $fixEmail   = $this->stringErrors( $list[$colEmails] );
                $checkEmail = $this->checkDns( $fixEmail );
            }else{
                $fixEmail = "email@invalido.dwp";
                $checkEmail = "Inválido";
            }

            // Remove column "Email" and send it from end of line
            unset( $list[$colEmails] );
            $temp = implode( ",", $list ) .",".$fixEmail.",".$checkEmail;

            array_push( $saveNewList, utf8_decode( $temp ) );
        }

        $this->createNewCsv( $file_name, $saveNewList );

    }

    public function createNewCsv( $file_name, $fields )
    {
        $config = $this->getConfig("DefaultDir");
        $fp = fopen( $config['new'] . "new_".$file_name, 'w+' );

        foreach ( $fields as $v ){
            fputcsv( $fp, explode( ",", $v ) );
        }

        fclose( $fp );

        rename( $config['files'].$file_name, $config['files']."ok_".$file_name );

        echo "Arquivo gerado com sucesso";
    }

    public function resultPost( $file_name, $colEmails )
    {
        return $this->openFile( $file_name, $colEmails );
    }

}