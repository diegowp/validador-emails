<?php
    require_once "Email.php";
    $app = new \Dwp\Email\Email();
    $config = $app->getConfig("DefaultDir");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DWP - Valida Email</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery-3.2.1.min.js"></script>
</head>
<body>

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-4">

                <form action="" id="form-file" method="post">

                    <h4>Selecione o arquivo <small>( .csv )</small></h4>

                    <div class="form-group">
                        <label>Em qual coluna estão localizados os emails? <small>- começa em 0</small></label>
                        <input type="number" id="colEmail" name="colEmail" class="form-control" value="1" required>
                    </div>

                    <div class="form-group">
                        <label>Listas:</label>
                        <select name="file" id="file" class="form-control">
                            <?php foreach ( $app->getFiles() as $v ): ?>
                                <option value="<?=utf8_encode($v)?>"><?=utf8_encode($v)?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-success execute">
                        <span class="glyphicon glyphicon-refresh"></span> Executar
                    </button>
                </form>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <?php if( $_SERVER['REQUEST_METHOD'] == 'POST' ): ?>
                    <div class="modal fade" id="info-modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"> <span class="glyphicon glyphicon-info-sign"></span> Info!</h4>
                                </div>
                                <div class="modal-body">
                                    <h4><?php $app->resultPost( $_POST['file'], $_POST['colEmail'] ); ?></h4>
                                    <a href="<?php echo $config['new'].'new_' . $_POST['file']; ?>" class="btn btn-success">
                                        <span class="glyphicon glyphicon-download"></span> Download
                                    </a>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger close-modal" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        jQuery(document).ready( function ($) {
                            $("#info-modal").modal('show');

                            $(".close-modal").click(function(){
                                location.href = location.pathname;
                            });
                        } );
                    </script>
                <?php endif;?>
            </div>
        </div>

    </div>

    <!-- Bootstrap JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/main.js"></script>
</body>
</html>