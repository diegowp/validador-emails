# **Instruções**

### Requisitos

 - Instalação de um servidor para executar a aplicação PHP.
 - Noções básicas em planilhas para manusear arquivos CSV.
 - Uma garrafa de café quentinho.
 - Muita paciência =)

### **Observações:**

```

1 - Decimais devem ser separados por "." nunca por ","
2 - NÃO deve conter NENHUMA "," no corpo no documento
3 - Apenas utilize arquivos CSV separados por vírgula codificados em UTF-8
4 - NÃO utilize acentos ou caracteres especiais no NOME DOS ARQUIVOS
5 - Insira em qual coluna se encontra os emails, lembrando que a contagem começa em 0
6 - Evite utilizar documentos com muitas linhas vazias
7 - Geralmente planilhas sempre contém linhas fantasmas, certifique-se que o documento esteja livre delas

```
 
### **Arquivo de configuração:** ( config.ini )
 
Este arquivo contém a configuração de pastas e a coleção de palavras que é utilizada na correção dos emails.
 
```
 [DefaultDir]
 app     = PASTA_RAIZ_DA_APLICAÇÃO
 files   = PASTA_ONDE_OS_CSVs_ESTÃO_ARMAZENADOS
 new     = PASTA_ONDE_SERÃO_ARMAZENDOS_AS_LISTAS_PROCESSADAS
 
 [Emails]
 ERRADO = CORRETO ( ex: hotmail.com.br = hotmail.com  )
```
 
```
 - Desenvolvido por: Diego Teixeira
```